package com.cerotid.oo.basic;

public class EmployeeTestClass {

	public static void main(String[] args) {
		//How to make object?
		//keyword to create object == "new"
		
		//employee1 is an instance of an Employee class
		Employee employee1= new Employee();
		employee1.name = "David";
		employee1.salary = 100000.00;
		employee1.ssn = "123-10-1221";
		
		Employee employee2= new Employee();
		employee2.name = "Roger";
		employee2.salary = 200000.00;
		employee2.ssn = "993-10-9999";
		
		Employee employee3 = new Employee("Micahel", "123456789", 222333.0);
		
		
		System.out.println("Print 1st Employee Info");
		employee1.printEmployeeInfo();
		employee1.printSocialSecurityAndName();
		
		
		System.out.println("Print 2nd Employee Info");
		employee2.printEmployeeInfo();
		employee2.printSocialSecurityAndName();
		
		
		System.out.println("Print 3rd Employee Info");
		employee3.printEmployeeInfo();
		employee3.printSocialSecurityAndName();
		
		Employee[] empArray = new Employee[2];
		empArray[0] = employee1;
		empArray[1] = employee2;
		
		Employee[] empArray2 = {employee1 , employee2};
		
	}

}
