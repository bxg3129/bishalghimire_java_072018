package com.cerotid.oo.principles;

public class Dog extends Pet {
	//constructor
	Dog(){
		
	}
	
	//overloaded contructor
	Dog(boolean isInsured){
		super.setInsured(isInsured);
		super.setNumberOfteeth(42);
	}
	
	Dog(boolean isSick, String color ){
		super.setSick(isSick);
		super.setColor(color);
	}
	
	@Override
	public void eat() {
		System.out.println("Dig treats, if I can!");
	}
	
	public void eat(String food) {
		System.out.println("I am eating "+ food);
	}

	@Override
	public void sleeptime() {
		System.out.println("Its my wish to sleep whenever I want!");
		
	}
}
