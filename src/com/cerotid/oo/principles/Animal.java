package com.cerotid.oo.principles;

public abstract class Animal {
	private String color;
	private final int limbs = 4;
	private int numberOfteeth;
	private boolean isSick;
	
	public abstract void sleeptime();
	
	public final void breathe() {
		System.out.println("Breathing in O2, breathing out CO2");
	}
	
	public void eat() {
		System.out.println("Eating food");
	}
	
	public void makeSound() {
		System.out.println("I don't know which sound to produce");
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getNumberOfteeth() {
		return numberOfteeth;
	}

	public void setNumberOfteeth(int numberOfteeth) {
		this.numberOfteeth = numberOfteeth;
	}

	public int getLimbs() {
		return limbs;
	}
	

	public boolean isSick() {
		return isSick;
	}

	public void setSick(boolean isSick) {
		this.isSick = isSick;
	}

	@Override
	public String toString() {
		return "Animal [color=" + color + ", limbs=" + limbs + ", numberOfteeth=" + numberOfteeth + ", isSick=" + isSick
				+ "]";
	}


	
}
