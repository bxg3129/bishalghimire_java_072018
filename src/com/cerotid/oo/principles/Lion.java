package com.cerotid.oo.principles;

public class Lion extends WildAnimal {
	@Override
	public void eat() {
		System.out.println("Try me!! I will eat you!");
		super.eat();
	}
	
	@Override
	public void lives() {
		System.out.println("You kidding me? I am a king of Jungle");
	}

	@Override
	public void sleeptime() {
		System.out.println("I am the king, I make my own time to sleep");
		
	}
	
}
