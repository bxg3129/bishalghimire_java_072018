package com.cerotid.oo.principles;

import com.cerotid.oo.animalaction.Veterinary;

public class AnimalTest {

	public static void main(String[] args) {
		// Creating Animal Object
		System.out.println("I am first Animal");
		Animal animal1 = new Zebra();
		animal1.breathe();
		animal1.eat();
		animal1.makeSound();

		// Create Wild Animal Object
		System.out.println("I am first Wild Animal");
		WildAnimal wildAnimal1 = new Zebra();
		wildAnimal1.breathe();
		wildAnimal1.eat();
		wildAnimal1.makeSound();
		wildAnimal1.lives();

		System.out.println();
		System.out.println("I am a Lion");
		Lion lion = new Lion();
		lion.eat();

		// Animal = Dog which is also an Animal
		Animal animal2 = new Dog();
		Animal animal3 = wildAnimal1;

		Dog dog = new Dog(true, "white");

		System.out.println("I am insured: " + dog.isInsured() + " & I have this many teeth: " 
							+ dog.getNumberOfteeth());
		// Dog != Animal , because Animal is not necessarily a dog
		// Dog dog = new Animal();
		
		Animal cowAnimal = new Cow();
		cowAnimal.eat(); 
		
		Cow cow = new Cow();
		cow.eat();
		
		Animal aCow = cow;
		
		Object a = aCow;
		
		Veterinary veterinary = new Veterinary();
		veterinary.treatAnimal(dog);
		veterinary.treatAnimal(cow);
		veterinary.treatAnimal(lion);
		veterinary.treatAnimal(wildAnimal1);
		
		veterinary.displayVendingMachine();
	}

}
