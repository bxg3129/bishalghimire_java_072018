package com.cerotid.oo.principles;

public class DomesticAnimal extends Animal {
	public void lives() {
		System.out.println("I live around people");
	}

	// overriding
	public void eat() {
		System.out.println("I eat whatever people allows me to eat");
	}

	@Override
	public void sleeptime() {
		System.out.println("Whenever I am not eating");
		
	}
	
	
}
