package com.cerotid.oo.principles;

public abstract class Pet extends Animal {
	private boolean isInsured;
	
	public void lives() {
		System.out.println("I live with People in their house");
	}

	// overriding
	public void eat() {
		System.out.println("I eat expensive food");
	}

	public boolean isInsured() {
		return isInsured;
	}

	public void setInsured(boolean isInsured) {
		this.isInsured = isInsured;
	}
	
	
	
}
