package com.cerotid.oo.principles;

import com.cerotid.oo.interfaceconcept.VendingMachine;

public class Hotel implements VendingMachine{
	private int numOfRooms;
	private boolean gym;
	private boolean pool;
	
	public int getNumOfRooms() {
		return numOfRooms;
	}
	public void setNumOfRooms(int numOfRooms) {
		this.numOfRooms = numOfRooms;
	}
	public boolean isGym() {
		return gym;
	}
	public void setGym(boolean gym) {
		this.gym = gym;
	}
	public boolean isPool() {
		return pool;
	}
	public void setPool(boolean pool) {
		this.pool = pool;
	}
	@Override
	public void displayVendingMachine() {
		System.out.println("Want to buy Sodas?");
		
	}
	
	
}
