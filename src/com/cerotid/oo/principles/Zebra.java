package com.cerotid.oo.principles;

public final class Zebra extends WildAnimal {
	@Override
	public void eat() {
		System.out.println("Even though I am wild, I just eat grains and leaves");
	}
	
	@Override
	public void lives() {
		System.out.println("I live in Jungle; but scared of Lion");
	}

	@Override
	public void sleeptime() {
		System.out.println("When I feel like I am protected");
		
	}
	
	
	
}
