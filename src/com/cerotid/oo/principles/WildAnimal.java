package com.cerotid.oo.principles;

public abstract class WildAnimal extends Animal {
	public void eat() {
		super.eat();
		System.out.println("I am eating from WildAnimal class");
	}
	
	public void lives() {
		System.out.println("I am from Jungle");
	}

	
}
