package com.cerotid.oo.interfaceconcept;

public class CokeVendingMachine implements VendingMachine {

	@Override
	public void displayVendingMachine() {
		System.out.println("I Vend Coke products!");
	}

}
