package com.cerotid.oo.interfaceconcept;

import com.cerotid.oo.principles.Animal;

public interface PetsmartFranchiser {
	public void registerAnimal(Animal animal);
	public Animal treatAnimal(Animal animal) ;
	public Animal groomAnimal(Animal animal);
}
