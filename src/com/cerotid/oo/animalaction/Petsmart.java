package com.cerotid.oo.animalaction;

import com.cerotid.oo.interfaceconcept.PetsmartFranchiser;
import com.cerotid.oo.interfaceconcept.VendingMachine;
import com.cerotid.oo.principles.Animal;

public class Petsmart implements  PetsmartFranchiser{
	private VendingMachine vendingMachineFranchiser;
	
	Petsmart (VendingMachine vendingMachineFranchiser){
		this.vendingMachineFranchiser = vendingMachineFranchiser;
	}
	
	@Override
	public void registerAnimal(Animal animal) {
		System.out.println(animal.getClass().getSimpleName()  + "registrered Petsmart way");
		
	}

	@Override
	public Animal treatAnimal(Animal animal) {
		// TODO Auto-generated method stub
		System.out.println( animal.getClass().getSimpleName()  + " treated Petsmart way");
		return animal;
	}

	@Override
	public Animal groomAnimal(Animal animal) {
		// TODO Auto-generated method stub
		System.out.println(animal.getClass().getSimpleName()  + " treated Petsmart way");
		
		return null;
	}

	public VendingMachine getVendingMachineFranchiser() {
		return vendingMachineFranchiser;
	}

	public void setVendingMachineFranchiser(VendingMachine vendingMachineFranchiser) {
		this.vendingMachineFranchiser = vendingMachineFranchiser;
	}
	
	
}
