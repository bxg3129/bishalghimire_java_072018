package com.cerotid.oo.animalaction;

import com.cerotid.oo.interfaceconcept.PetsmartFranchiser;
import com.cerotid.oo.interfaceconcept.VendingMachine;
import com.cerotid.oo.principles.Animal;
import com.cerotid.oo.principles.Cat;
import com.cerotid.oo.principles.Cow;
import com.cerotid.oo.principles.Dog;

public class Veterinary implements PetsmartFranchiser, VendingMachine{
	
	
	/*public Dog treatAnimal(Dog dog){
		//TODO treatment logic
		System.out.println("Dog is treated");
		
		return dog;
	}
	
	public Cat treatAnimal (Cat cat) {
		System.out.println("Cat is treated");
		
		return cat;
	}
	
	public Cow treatAnimal(Cow cow) {
		System.out.println("Cow is treated");
		
		return cow;
	}*/
	
	public Animal treatAnimal(Animal animal) {
		
		//instanceof
		
		if(animal instanceof Dog) {
			Dog dog = (Dog) animal;
			
			treatDog(dog);
			System.out.println("Dog is treated ");
		}
		else if (animal instanceof Cat)
			System.out.println("Cat is treated");
		else if (animal instanceof Cow)
			System.out.println("Cow is treated");
		else
			System.out.println("Sorry! " + animal.getClass().getSimpleName() + " cannot be treated here!");
		
		return animal;
	}

	private void treatDog(Dog dog) {
		tieDog();
		diagnose();
		
		if(dog.isSick()) {
			xray();
			inject();
			prescribeMedicine();
		}
	}

	private void prescribeMedicine() {
		// TODO Auto-generated method stub
		
	}

	private void inject() {
		// TODO Auto-generated method stub
		
	}

	private void xray() {
		// TODO Auto-generated method stub
		
	}

	private void diagnose() {
		// TODO Auto-generated method stub
		
	}

	private void tieDog() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerAnimal(Animal animal) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Animal groomAnimal(Animal animal) {
		// TODO Auto-generated method stub
		System.out.println("Your animal is groomed");
		return animal;
	}

	@Override
	public void displayVendingMachine() {
		// TODO Auto-generated method stub
		System.out.println("I am vending Coke & Energy Drink");
	}



}
