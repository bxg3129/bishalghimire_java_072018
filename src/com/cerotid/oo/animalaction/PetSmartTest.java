package com.cerotid.oo.animalaction;

import com.cerotid.oo.interfaceconcept.CokeVendingMachine;
import com.cerotid.oo.interfaceconcept.MuscleMilkVendingMachine;
import com.cerotid.oo.interfaceconcept.VendingMachine;

public class PetSmartTest {
	public static void main(String[] args) {
		VendingMachine vendingMachine = new CokeVendingMachine();
		
		Petsmart petsmart = new Petsmart(vendingMachine);
		petsmart.getVendingMachineFranchiser().displayVendingMachine();
		
		VendingMachine muscleMilkVendignMachine = new MuscleMilkVendingMachine();
		
		petsmart.setVendingMachineFranchiser(muscleMilkVendignMachine);
		
		petsmart.getVendingMachineFranchiser().displayVendingMachine();
	}
}
