package com.cerotid.general;

import java.util.Scanner;

public class StaticConcept {
	static String name = "David";
	int age = 30;
	
	public static void main(String[] args) {
		StaticConcept sc1 = new StaticConcept();
		sc1.name="Michael";
		sc1.age = 25;
		
		printStaticConceptDetails("SC1::",sc1.name, sc1.age);
		
		StaticConcept sc2 = new StaticConcept();
		sc2.name="Rakesh";
		
		printStaticConceptDetails("SC2::",sc2.name, sc2.age);
		
		printStaticConceptDetails("SC1::",sc1.name, sc1.age);
		
		//In order to access static variable or methods,
		//you don't have to instantiate
		
		Scanner sc = ScannerInputConcept.input;
	}

	private static void printStaticConceptDetails(String objectName, String name, int age) {
		System.out.println("Object: "+ objectName);
		System.out.println("Name: "+ name);
		System.out.println("Age: "+ age);
		
	}
	

}
