package com.cerotid.general;

import java.util.Comparator;

import com.cerotid.bank.model.Customer;

public class ComparatorConcept implements Comparator <Customer>{

	@Override
	public int compare(Customer o1, Customer o2) {
		/*if(o1.getLastName().compareTo(o2.getLastName()) == 0){
			return o1.getFirstName().compareTo(o2.getFirstName());
		}*/
		
		return o1.getLastName().compareTo(o2.getLastName());
	}


}
