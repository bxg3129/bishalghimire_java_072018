package com.cerotid.general;

public class LoopConcept {
	public static void main(String[] arg) {
		//Types of loop
		//For loop
		//while loop
		//Do while loop
		//foreach
		
		//for loop using variable
		//0, 1, 2, 3, 4, 
		for(int i = 0; i < 5; i++ ) {
			System.out.println("My name is Bishal");
		}
		
		int j = 0;
		
		//while loop
		while (j< 5) {
			System.out.println("I am in Java class");
			
			j++; //j = j+1;
			
			System.out.println("Value of j="+ j);
		}
		
		int k = 0;
		//do while
		do {
			System.out.println("I am in a path to be a programmer");
			
			k = k + 1; //k++
			
		}while(k < 5);
		
	}
}
