package com.cerotid.general;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerInputConcept {
	static Scanner input;

	public static void main(String[] args) {
		int i = 1;
		
		getScannerInput();
		
		do {
			System.out.println("Enter you name: ");

			getScannerInput();
			String userName = input.nextLine();

			printUserInput(userName);
			
			
			System.out.println("Enter your age: ");
			
			int age = 0;
			while(true) {
				try {
					getScannerInput();
					age = input.nextInt();
					
					break; //breaks the immediate loop
				}
				catch (InputMismatchException e){
					System.out.println("Don't be stupid! Provide integer value");
				}
			}
			
			printUserInput(age);
			
			i++;

		} while (i <= 5);

	}
	
	private static void getScannerInput() {
			input = new Scanner(System.in);
	}

	private static void printUserInput(Object userName) {

		System.out.println("You entered: " + userName);
	}

}
