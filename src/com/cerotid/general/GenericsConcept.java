package com.cerotid.general;

import java.util.ArrayList;

import com.cerotid.oo.basic.Employee;

public class GenericsConcept {

	public static void main(String[] args) {
		String name1 = "Randy";
		String name2 = "David";
		String name3 = "Sunil";
		
		int age1 = 12;
		int age2 = 35;
		int age3 = 45;
		
		ArrayList trashCan = new ArrayList();
		trashCan.add(name1);
		trashCan.add(name2);
		trashCan.add(name3);
		
		trashCan.add(age1);
		trashCan.add(age2);
		trashCan.add(age3);
		
		trashCan.add(true);
		
		ArrayList<String> stringCan = new ArrayList <String>();
		stringCan.add(name1);
		stringCan.add(name2);
		stringCan.add(name3);
		
		//Below lines are not fixed to show you stringCan can only take String type
	/*	stringCan.add(age1);
		stringCan.add(age2);
		stringCan.add(age3);
		stringCan.add(true);*/
		
		Employee emp1 = new Employee();
		Employee emp2 = new Employee();
		Employee emp3 = new Employee();
		Employee emp4 = new Employee();
		Employee emp5 = new Employee();
		
		ArrayList<Employee> employeeList = new ArrayList<>();
		employeeList.add(emp1);
		employeeList.add(emp2);
		employeeList.add(emp3);
		employeeList.add(emp4);
		employeeList.add(emp5);
		
		//Below line is not fixed to show you employeeList can only take Employee type
		//employeeList.add("David");
	}

}
