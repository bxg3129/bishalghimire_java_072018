package com.cerotid.general;

public class Driver {
	private static final String NAME = "Michael";
	private static final String SSN = "123456789";
	private static final String STATE_CODE="OH";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EncapsulationConcept enc = new EncapsulationConcept();
		
		//enc.name = "Micahel";
		enc.setName(NAME);
		
		//enc.ssn = "123-990000-1000";
		enc.setSsn(SSN);
		
		System.out.println("Name ="+ enc.getName());
		
		enc.printEmployeinfo();

	}

}
