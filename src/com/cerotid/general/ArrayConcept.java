package com.cerotid.general;

public class ArrayConcept {
	public static void main(String[] args) {
		//ArrayConcept
		
		int [] arrayInteger =new int[10];
		
		arrayInteger[0]= 100;
		arrayInteger[5]= 500;
		arrayInteger[8] = 200;
		arrayInteger[1] = 50;
		
		arrayInteger[9] = 1000;
		
	
		for(int i = 0; i < 10; i++) {
			System.out.println("Value in Array position " + i + ":" + arrayInteger[i]);
		}
		
		System.out.println("Size of array : " + arrayInteger.length);
	}

}
