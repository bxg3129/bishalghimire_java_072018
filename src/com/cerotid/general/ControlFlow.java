package com.cerotid.general;

public class ControlFlow {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Example of IF-Else if-Else
		int age = 67;
		
		if(age < 5) {
			System.out.println("Send to Day Care");
		}
		else if(age >= 5 && age < 18) {
			System.out.println("Go to school");
		}
		else if(age >= 18 && age < 24) {
			System.out.println("Go to College");
		}
		else {
			System.out.println("You don't have life!~");
		}

		//If
		
		if(age >= 65) {
			System.out.println("You can now get Social security benefit!");
		}
		
		
		//Expresseion   
		//? then
		//else :
		String seniority = (age >= 65) ? "Senior" : "Not yet senior";
		
		//equivalent to above
		if(age >= 65) {
			seniority = "Senior";
		}
		else
			seniority = "Not yet senior";
		
		//switch case
		
		String sports = "Cricket";
		String fanType = "Crazy";
		
		switch (sports) {
		case "Football":
			
			System.out.println("I love football");
			break;

		case "Baseball":
			System.out.println("I love Baseball");
			break;	
		
		case "Cricket":
			System.out.println("I love Cricket");
			break;
			
		default:
			System.out.println("Hey, I don't like any sports");
			break;
		}
		
		
	}

}
