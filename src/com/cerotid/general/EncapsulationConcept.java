package com.cerotid.general;

//hiding the information
public class EncapsulationConcept {
	private String name;
	private String ssn;
	
	void printEmployeinfo(){
		System.out.println("Name: " + name + " SSN: " + ssn);
	}
	
	//setter - Mutator
	public void setName(String x) {
		name = x;
	}

	public void setSsn(String ssn) {
		
		if(ssn.length() != 9) {
			this.ssn = null;
			return;
		}
		
		this.ssn = ssn;
	}

	public String getName() {
		return name;
	}

	public String getSsn() {
		return ssn;
	}
	
	
	//getter - Accessors
	

}
