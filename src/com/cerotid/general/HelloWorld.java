package com.cerotid.general;

public class HelloWorld {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello World!");
		
		System.out.println("This is my first programming exercise!");
		
		
		//variable
		//something to assign on left, value on right
		
		//primitives
		
		short a = 2;
		int x = 5;
		long ln = 22222222;
		double y= 5.5;
		float dn = 454.34f;
		char c = 'd';
		
		//operations
		x = x + 5;
		
		System.out.println("Variable of x " + x);
		
		x++; // x = x + 1;
		
		System.out.println("Variable of x " + x);
		
		x--; //x = x - 1;
		System.out.println("Variable of x " + x);
		
		//conditions //control flow
		//if <Statement True>  [Do this] Else if <Statement True> [Do that] For everything else {dp something else}
		int age = 30;
		
		if(age < 5) {
			System.out.println("Send to Day Care");
		}
		else if(age >= 5 && age < 18) {
			System.out.println("Go to school");
		}
		else if(age >= 18 && age < 24) {
			System.out.println("Go to College");
		}
		else {
			System.out.println("You don't have life!~");
		}
	}

}
