package com.cerotid.general;

public enum EnumConcept {
	SUMMER ("summer"), WINTER ("winter"), FALL("fall"), SPRING ("spring");
	
	private final String weather;
	
	EnumConcept(String a){
		this.weather = a;
	}

	public String getWeather() {
		return weather;
	}
	
	
}
