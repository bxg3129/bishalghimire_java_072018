package com.cerotid.general;

public class Calculator {
	//class level variables - hold state
	int result;
	
	
	
	
	//methods are behaviors
	//sum
	int sum(int a, int b){
		return a+b;	
	}
	
	int sum(int a, int b, int c){
		return a+b+c;	
	}
	
	void multiply(int a, int b){
		//local variable
		//int random = 9;
		
		result = a * b;
	}
	//substract
	//divide
	//multiply
	//sqrRt
	//
	
	public static void main(String[] args) {
		//instantiation
		//Create calculator object (i.e new Calculator()) and assign to a variable of type Calculator
		Calculator myFirstObject = new Calculator();
		
		int sum = myFirstObject.sum(100, 200);
		System.out.println(sum);
		
		sum = myFirstObject.sum(10, 20, 30);
		System.out.println(sum);
		
		myFirstObject.multiply(2, 10);
		
		System.out.println(myFirstObject.result);
		
		Calculator mySecondObject  = new Calculator();
		
		Calculator myThirdObject = new Calculator();
	}
}
