package com.cerotid.general;

public class InnerClassConcept {

	public static void main(String[] args) {
		InnerClassConcept inc= new InnerClassConcept();
		
		//instantiate inner class B
		InnerB innerClassBObject = inc.new InnerB();
		innerClassBObject.accessInnerPropertyOfB();
		
		InnerA.accessInnerProperty();
	}
	
	private static class InnerA{
		public static void accessInnerProperty() {
			
		}
	}
	
	private class InnerB{
		public void accessInnerPropertyOfB() {
			
		}
	}
}
