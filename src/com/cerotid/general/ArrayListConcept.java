package com.cerotid.general;

import java.util.ArrayList;

public class ArrayListConcept {
	public static void main(String[] args) {
		ArrayList aList = new ArrayList();
		
		aList.add("Cat");
		aList.add("Cat");
		aList.add(new Integer(2));
		aList.add(1);
		aList.add("Dog");
		aList.add("Ball");
		aList.add(100);
		aList.add(new Double("10.0")); //will figure out why???
		aList.add("Bat");
		
		arrayListPrinter(aList);
		
		newLine();
		System.out.println(aList.get(4));
		newLine();
		
		aList.remove("Ball");
		
		newLine();
		
		arrayListPrinter(aList);
		
		ArrayList bList = new ArrayList();
		bList.add("Yacht");
		bList.add(new Integer(2500));
		bList.add(1.0);
		bList.add("Ship");
		bList.add("Helmet");
		bList.add(1000000);
		
		aList.addAll(bList);
		
		newLine();
		arrayListPrinter(aList);
	}
	
	static void newLine() {
		System.out.print("\n");
	}
	
	static void arrayListPrinter(ArrayList aList) {
		Integer i = 0;
		String a = null; 
		
		for (Object object : aList) {
			if (object instanceof String) {
				a = (String) object;
				
				System.out.println(a);
			}
			else if (object instanceof Integer)
			{
				i = (Integer) object;
				System.out.println(i);
			}
		}
	}
}
