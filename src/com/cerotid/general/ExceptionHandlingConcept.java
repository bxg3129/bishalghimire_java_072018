package com.cerotid.general;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ExceptionHandlingConcept {
	public static void main(String[] args) {
		try {
			
		
		FileReader reader = null;
		FileWriter writer = null;
		
		reader = new FileReader("inputText.txt");
		writer = new FileWriter("outputText.txt");
		
		int character;
		while((character = reader.read()) != -1) {
			System.out.println(character);
			writer.write(character);
		}
		
		reader.close();
		writer.close();
		
		}catch(IOException e) {
			System.out.println("Input file is not submitted");
		}
	}

}
