package com.cerotid.bank.model;

public enum CheckType {
	PAPER_CHECK, E_CHECK
}
