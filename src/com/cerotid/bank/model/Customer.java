package com.cerotid.bank.model;

import java.util.ArrayList;
import java.util.Comparator;

public class Customer{
	private String firstName;
	private String lastName;
	private ArrayList<Account> accounts;
	private String address;
	private String ssn;
	private String dob;
	
	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public Customer() {
		
	}
	
	public Customer(String fname, String lastName) {
		this.firstName = fname;
		this.lastName = lastName;
		
		if(this.accounts == null) {
			this.accounts = new ArrayList<>();
		}
	}
	
	public Customer(String fname, String lastName, String ssn, String address) {
		this.firstName = fname;
		this.lastName = lastName;
		this.ssn = ssn;
		this.address = address;
		
		if(this.accounts == null) {
			this.accounts = new ArrayList<>();
		}
	}
	
	public Customer(String fname, String lastName, String ssn, String dob, String address) {
		this.firstName = fname;
		this.lastName = lastName;
		this.ssn = ssn;
		this.address = address;
		this.dob = dob;
		
		if(this.accounts == null) {
			this.accounts = new ArrayList<>();
		}
	}
	
	//Accessors Mutators
	public ArrayList<Account> getAccounts() {
		return accounts;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	void printCustomerAccounts(){
		//TODO print customer Accounts
	}
	
	void printCustomerDetails(){
		System.out.println(toString());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (ssn == null) {
			if (other.ssn != null)
				return false;
		} else if (!ssn.equals(other.ssn))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Customer [firstName=" + firstName + ", lastName=" + lastName + ", accounts=" + accounts + ", address="
				+ address + "]";
	}
	
	public static Comparator<Customer> LastNameComparator =
			new Comparator<Customer>() {

				@Override
				public int compare(Customer o1, Customer o2) {
					if(o1.getLastName().compareTo(o2.getLastName()) == 0){
						return o1.getFirstName().compareTo(o2.getFirstName());
					}
					
					return o1.getLastName().compareTo(o2.getLastName());
				}
		
	};
	
	public static Comparator<Customer> DOBComparator =
			new Comparator<Customer>() {

				@Override
				public int compare(Customer o1, Customer o2) {
					return o1.getDob().compareTo(o2.getDob());
				}
	};
	
	
	//add accounts
	public void addAccount(Account account) {
		this.accounts.add(account);
	}
	
}
