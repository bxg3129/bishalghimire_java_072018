package com.cerotid.bank.model;

public abstract class Transaction {
		/*Variables- 
		- amount
		- fee
		- receiver firstName
		- receiver lastName
	
		createTransaction()
		deductAccountBalance()*/
	private double amount;
	private double fee;
	private String firtName;
	private String lastName;
	
	void createTransaction() {
		//TODO create Transaction
	}
	
	void deductAccountBalance() {
		//TODO add deduct logic
	}
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	public String getFirtName() {
		return firtName;
	}
	public void setFirtName(String firtName) {
		this.firtName = firtName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
