package com.cerotid.bank.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import com.cerotid.general.ComparatorConcept;

public class CustomerReader {
	//--declare customer storage bucket
	private ArrayList<Customer> customerList = new ArrayList<>();
	
	//--create method to read the file
	public void readFile() throws IOException {
		//find the file, open file
		File file = new File("CustomerInput.txt");
		
		FileReader fileReader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		String line;
		
		//read line and 
		  //loop through the lines until end of file
		while ((line = bufferedReader.readLine()) != null) {
			ArrayList<String> list = new ArrayList<String>(Arrays.asList(line.split(",")));
			
			String firstName = list.get(1);
			String lastName = list.get(0);
			
			Customer customer = new Customer(firstName, lastName);
			
			//store as single customer in the storage bucket
			customerList.add(customer);
		}
		  //close filereader
		bufferedReader.close();
		fileReader.close();
		System.out.println("Contents of file:");
	}
	
	void printCustomerAccounts(){
		//TODO print customer Accounts
	}
	
	//create writer file
	public void writeFile() throws IOException {
		//create file
		File file = new File("CustomerSortedOutput.txt");
		
		FileWriter filerWriter = new FileWriter(file);
		
		//sortCustomers();
		sortCustomersWithTheirLastNames();
		
		BufferedWriter bufferedWriter = new BufferedWriter(filerWriter);
		
		//write one by one in the file
		for (Customer cust: customerList) {
			bufferedWriter.write(cust.getFirstName());
			bufferedWriter.write(" ");
			bufferedWriter.write(cust.getLastName());
			bufferedWriter.write("\n");
		}
		
		//close
		bufferedWriter.close();
		filerWriter.close();
	}

	/*private void sortCustomers() {
		//TODO sort customer storage bucket in ascending order by their last name
		ArrayList<String> lastNameList = new ArrayList<>();
		
		for(Customer customer: customerList) {
			lastNameList.add(customer.getLastName());
		}
		
		Object[] arr = (Object[]) lastNameList.toArray();
	
		Arrays.sort(arr);
		
		for(Object lastName: arr) {
			System.out.println(lastName);
		}
	}*/
	
	private void sortCustomersWithTheirLastNames() {
		Collections.sort(customerList, new ComparatorConcept());
		
		for (Customer cust: customerList) {
			System.out.println(cust.getLastName());
		}
	}
	
}
