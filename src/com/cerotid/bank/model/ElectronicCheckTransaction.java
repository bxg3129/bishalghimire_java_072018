package com.cerotid.bank.model;

public class ElectronicCheckTransaction extends Transaction {
	//- typeOfCheck: enum [papercheck, eCheck]
	private CheckType checktype;

	public CheckType getChecktype() {
		return checktype;
	}

	public void setChecktype(CheckType checktype) {
		this.checktype = checktype;
	}
	
	
	
}
