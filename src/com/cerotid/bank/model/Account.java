package com.cerotid.bank.model;

import java.io.Serializable;
import java.util.Date;


/**
 * @author Bishal
 *
 */
public class Account implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5965307115529503294L;
	
	private AccountType accountType;
	private Date accountOpenDate;
	private Date accountCloseDate;
	private double amountBalance;
	
	public Account(){
		
	}
	
	public Account(AccountType accountType, Date accntOpenDate, double openingBalance) {
		this.accountType = accountType;
		this.accountOpenDate = accntOpenDate;
		this.amountBalance = openingBalance;
	}
	
	public void sendMoney() {
		//TODO sendMoney service call
		//TODO once above action is successul, adjust amountBalance
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	void printAccountInfo(){
		System.out.println(toString());
	}
	
	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public Date getAccountCloseDate() {
		return accountCloseDate;
	}

	public void setAccountCloseDate(Date accountCloseDate) {
		this.accountCloseDate = accountCloseDate;
	}

	public double getAmountBalance() {
		return amountBalance;
	}

	public void setAmountBalance(double amountBalance) {
		this.amountBalance = amountBalance;
	}

	@Override
	public String toString() {
		return "Account [accountType=" + accountType + ", accountOpenDate=" + accountOpenDate + ", accountCloseDate="
				+ accountCloseDate + ", amountBalance=" + amountBalance + "]";
	}
}
