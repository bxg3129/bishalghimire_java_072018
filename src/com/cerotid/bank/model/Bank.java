package com.cerotid.bank.model;

import java.util.ArrayList;

public class Bank {
	private final String bankName = "ANC bank";
	private ArrayList<Customer> customers;
	
	public Bank() {
		this.customers = new ArrayList<>();
	}
	
	//Required Accessor mothods and Modifier method created
	
	public ArrayList<Customer> getCustomers() {
		return customers;
	}
	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
	public String getBankName() {
		return bankName;
	}
	
	public void printBankName(){
		System.out.println(getBankName());
	}
	
	public void printBankDetails(){
		//TODO Add print bank details logic
		System.out.println(toString());
		
		
	}
	@Override
	public String toString() {
		return "Bank [bankName=" + bankName + ", customers=" + customers + "]";
	}
	
	//New Methods Assignment 5
	public void addCustomer(Customer customer) {
		customers.add(customer);
	}
	
	public void editCustomerCustomerInfo(Customer customer) {
		//TODO Add edit Customer logic
	}
	
	public Customer getCustomer(String ssn) {
		for(Customer customer: customers) {
			if(ssn.equals(customer.getSsn())) {
				return customer;
			}
		}
		
		return null;
		
	}
}
