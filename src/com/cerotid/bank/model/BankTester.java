package com.cerotid.bank.model;

import java.io.IOException;
import java.util.ArrayList;

public class BankTester {

	public static void main(String[] args) throws IOException {
		Bank bank = new Bank();
		
		Account customer1acct1 = new Account();
		customer1acct1.setAccountType(AccountType.CHECKING);
		
		Account customer1acct2 = new Account();
		customer1acct2.setAccountType(AccountType.BUSINESS_CHECKING);
		
		Account customer1acct3 = new Account();
		customer1acct3.setAccountType(AccountType.SAVINGS);
		
		ArrayList<Account> customer1Accounts = new ArrayList<>();
		customer1Accounts.add(customer1acct1);
		customer1Accounts.add(customer1acct2);
		customer1Accounts.add(customer1acct3);
		
		Customer customer1 = new Customer();
		customer1.setAddress("111 Main st, NY, 12345");
		customer1.setFirstName("David");
		customer1.setLastName("Smith");
		customer1.setAccounts(customer1Accounts);
		
		Customer customer2 = new Customer();

		
		Customer customer3 = new Customer();
		
		ArrayList<Customer> customerList = new ArrayList<>();
		customerList.add(customer1);
		customerList.add(customer2);
		customerList.add(customer3);
		
		bank.setCustomers(customerList);
		
		bank.printBankDetails();
		bank.printBankName();
		
		
		CustomerReader csr = new CustomerReader();
		csr.readFile();
		
		csr.writeFile();
	}

}
