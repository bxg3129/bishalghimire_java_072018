package com.cerotid.bank.model;

public class MoneyGramTransaction extends Transaction {
	/*-deliveryOptions: Enum [10Minute, 24HRS]
			-destinationCountry*/
	private DeliveryOption deliveryOption;
	private String destinationCountry;
	
	public DeliveryOption getDeliveryOption() {
		return deliveryOption;
	}
	public void setDeliveryOption(DeliveryOption deliveryOption) {
		this.deliveryOption = deliveryOption;
	}
	public String getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
}
