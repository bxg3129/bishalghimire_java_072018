package com.cerotid.bank.model;

public enum AccountType {
	CHECKING, SAVINGS,BUSINESS_CHECKING
}
