package com.cerotid.bank.controller;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;

public interface BankingInterface {
	public void addCustomer(Customer customer);
	
	public boolean openAccount(Customer customer, Account account);
	
	public void sendMoney(Customer customer, Transaction transaction);
	
	public void depositMoneyInCustomerAccount(Customer customer);
	
	public void editCustomerInfo(Customer customer);
	
	public Customer getCustomerInfo(String ssn);
	
	public void printBankStatus();
}
