package com.cerotid.bank.controller;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Bank;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;

public class BankController implements BankingInterface{
	private static Bank bank;
	
	static {
		bank = new Bank();
	}

	@Override
	public void addCustomer(Customer customer) {
		bank.getCustomers().add(customer);
	}

	@Override
	public boolean openAccount(Customer customer, Account account) {
		// TODO Auto-generated method stub
		if(bank.getCustomers().contains(customer)) {//Check if the customer exist in the Bank!
			int i =  bank.getCustomers().indexOf(customer);
			
			bank.getCustomers().get(i).addAccount(account);
			
			return true;
		}
		else {
			System.out.println("Customer do not exist!");
			
			return false;
		}
	}

	@Override
	public void sendMoney(Customer customer, Transaction transaction) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void depositMoneyInCustomerAccount(Customer customer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void editCustomerInfo(Customer customer) {
		// TODO Auto-generated method stub
		
	}
	
	public void printBankStatus() {
		System.out.println(bank);
	}

	@Override
	public Customer getCustomerInfo(String ssn) {
		return bank.getCustomer(ssn);
	}

}
