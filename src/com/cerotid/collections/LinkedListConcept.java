package com.cerotid.collections;

import java.util.LinkedList;

public class LinkedListConcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList<String> linkedList = new LinkedList<>();
		
		linkedList.add("Jeenu");
		linkedList.add("Ranjit");
		linkedList.add("Sanjiv");
		linkedList.add("Rakesh");
		linkedList.add("Bishal");
		linkedList.add("Sunil");
		
		System.out.println(linkedList);
		
		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());
		System.out.println(linkedList.poll());
		
		System.out.println(linkedList.poll());
	}

}
