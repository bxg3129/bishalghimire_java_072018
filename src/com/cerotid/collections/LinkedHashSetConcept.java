package com.cerotid.collections;

import java.util.LinkedHashSet;
import java.util.TreeSet;

public class LinkedHashSetConcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
		
		linkedHashSet.add("Rakesh");
		linkedHashSet.add("Rakesh");
		linkedHashSet.add("David");
		linkedHashSet.add("Sunil");
		linkedHashSet.add("Sanjiv");
		
	/*	//Can add null element as well
		set.add(null);
		set.add(null);
		*/
		System.out.println(linkedHashSet);
		
		//Converting LinkedHashSet to TreeSet
		
		TreeSet<String> tset = new TreeSet<>(linkedHashSet);
		System.out.println(tset);
	}

}
