package com.cerotid.collections;

import java.util.TreeSet;

public class TreeSetConcept {
	public static void main(String[] args) {
		TreeSet<String> set = new TreeSet<String>();
		
		set.add("Rakesh");
		set.add("Rakesh");
		set.add("David");
		set.add("Sunil");
		set.add("Sanjiv");
		
		System.out.println(set);
	}
}
