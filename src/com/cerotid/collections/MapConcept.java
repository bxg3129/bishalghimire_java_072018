package com.cerotid.collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.TreeMap;

public class MapConcept {

	public static void main(String[] args) {
		//HashMap key should be Immutable Object -- Read upon what is Immutable?
		//Example - String is Immutable Object
		//How to create Immutable Object? --Read upon
		HashMap<String, String> hashmap = new HashMap<String, String>();
		
		//insert
		hashmap.put("123456789", "David Smith");
		hashmap.put("123456789", "Kim Clark");
		hashmap.put("123456781", "Randy Dewey");
		hashmap.put("123456782", "Micahel Owen");
		hashmap.put("123456783", "Carla Graham");
		hashmap.put("123456784", "Dave Lewis");
		hashmap.put(null, "Some Name");
		hashmap.put(null, "Another name");
		
		//search
		System.out.println(hashmap.get("123456783"));
		System.out.println(hashmap.get("123456784"));
		System.out.println(hashmap.get("123456789"));
		System.out.println(hashmap.get(null));
		
		System.out.println(hashmap.keySet());
		
		LinkedHashMap<String, String> stateMap = new LinkedHashMap<String, String>();
		stateMap.put("CA", "California");
		stateMap.put("TX", "Texas");
		stateMap.put("UT", "Utah");
		stateMap.put("NY", "NewYork");
		stateMap.put("VA", "Virginia");
		stateMap.put("CO", "Colorado");
		
		System.out.println(stateMap.get("CA"));
		System.out.println(stateMap.keySet());
		System.out.println(stateMap.isEmpty());
		
		//sort
		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		treeMap.put("CA", "California");
		treeMap.put("TX", "Texas");
		treeMap.put("UT", "Utah");
		treeMap.put("NY", "NewYork");
		treeMap.put("VA", "Virginia");
		treeMap.put("CO", "Colorado");
		
		System.out.println(treeMap.keySet());
		
		
		Iterator iterator = treeMap.keySet().iterator();
		
		System.out.println("Iterating: ");
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}

}
