package com.cerotid.collections;

import java.util.PriorityQueue;

public class PriorityQueueConcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PriorityQueue<String> priorityQueue = new PriorityQueue<>();
		
		priorityQueue.add("Rakesh");
		priorityQueue.add("Rakesh");
		priorityQueue.add("David");
		priorityQueue.add("Sunil");
		priorityQueue.add("Ranjit");
		
		//while(priorityQueue.size() != 0	)
			//System.out.println(priorityQueue.poll());  //removes from the Head or return null if nothing available
		
		//removes from the Head or return null if nothing available
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
		
		
	}

}
