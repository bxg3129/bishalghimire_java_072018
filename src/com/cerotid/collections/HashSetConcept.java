package com.cerotid.collections;

import java.util.HashSet;

public class HashSetConcept {
	public static void main(String[] args) {
		HashSet<String> set = new HashSet<String>();
		
		set.add("Rakesh");
		set.add("Rakesh");
		set.add("David");
		set.add("Sunil");
		set.add("Sanjiv");
		
		//Can add null element as well
		set.add(null);
		set.add(null);
		
		System.out.println(set);
	}
}
