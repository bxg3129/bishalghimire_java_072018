package com.cerotid.concept.serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class SerilizeExceptionDemo {
	public static void main(String [] args) {
		Employee e;
		
		FileInputStream fileIn;
		try {
			fileIn = new FileInputStream("employee.ser");
		
			ObjectInputStream in = new ObjectInputStream(fileIn);
			e = (Employee) in.readObject();
			
		}
	
		catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("Hey provide me the file");
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println("Are you crazy? Provide right content is the file");
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("Your class was not understood");
			e1.printStackTrace();
		}
		catch(Exception el){
			System.out.println("Excepton caught");
		}
	}
}
