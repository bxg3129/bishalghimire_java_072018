package com.cerotid.concept.serialization;

import java.io.Serializable;

public class Employee implements Serializable {
	private Car car;
	/**
	 * 
	 */
	private static final long serialVersionUID = -438193571367027852L;
	/**
	 * 
	 */

	private  String name;
	private  String address;
	private  transient int SSN;
	private  int number;
	

	public Employee(String name, String address, int sSN, int number) {
		this.name = name;
		this.address = address;
		SSN = sSN;
		this.number = number;
	}
	
	public Employee(String name, String address, int sSN, int number, Car car) {
		this.name = name;
		this.address = address;
		SSN = sSN;
		this.number = number;
		this.car = car;
	}


	public void studentInfo() {
		System.out.println("Student Name:  " + name + " " + address);
	}


	public String getName() {
		return name;
	}


	public String getAddress() {
		return address;
	}


	public int getSSN() {
		return SSN;
	}


	public int getNumber() {
		return number;
	}
	

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	@Override
	public String toString() {
		return "Employee [car=" + car + ", name=" + name + ", address=" + address + ", SSN=" + SSN + ", number="
				+ number + "]";
	}
	

	/*@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Employee [name=").append(name).append(", address=").append(address);
		
		return sb.toString();
		
		return "Employee [name=" + name + ", address=" + address + ", SSN=" + SSN + ", number=" + number + "]";
	}
	
	*/
}
