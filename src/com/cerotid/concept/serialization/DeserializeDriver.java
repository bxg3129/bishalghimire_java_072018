package com.cerotid.concept.serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeDriver {

	public static void main(String[] args) {
		Employee e = null;
		try {
			FileInputStream fileIn = new FileInputStream("employee.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			e = (Employee) in.readObject();
			//StringTokenizer use tab deliminator
			//check first item for object creation
			//contractDoc.setName();
			
			//add to the EmployeeList
			in.close();
			fileIn.close();
			
			System.out.println(e);
			/*System.out.println("Right before system exit");
			System.exit(0);*/
		}
		catch (FileNotFoundException i) {
			System.out.println("I am here");
			i.printStackTrace();
			return;
		}
		catch (IOException i) {
			System.out.println("I am here2");
			i.printStackTrace();
			return;
		} 
		catch (ClassNotFoundException c) {
			System.out.println("Employee class not found");
			c.printStackTrace();
			return;
			
		}
		catch(Exception e1){
			e1.printStackTrace();
		}
		
		System.out.println(e.getCar());
	}
}
