package com.cerotid.concept.exceptionHandling;

public class SomeRunTimeException extends RuntimeException {
	
	public SomeRunTimeException(String message){
		super(message);
	}
}
