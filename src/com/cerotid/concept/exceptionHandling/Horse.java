package com.cerotid.concept.exceptionHandling;


public class Horse {
	private String name;
	private int age;
	
	public Horse(){
		
	}

	public Horse(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name){
		this.name = name;
		
		throw new SomeRunTimeException("HorseName not good");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public double getSpeed(){
		System.out.println("This is my default speed");
		return 10.0;
	}
	
	public String getColor(){
		return "Brown";
	}

	
}
