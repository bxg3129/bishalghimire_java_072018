package com.cerotid.concept.exceptionHandling;

import java.util.List;

public class Stable {
	public static void addHorse(List h, Horse horse) throws SomeException {
		if (h.size() >= Driver.storageMap.get("stable")) {
			throw new SomeException();
		} else {
			h.add(horse);
		}
	}
}
