package com.cerotid.concept.exceptionHandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Driver {
	public static Map<String, Integer> storageMap;
	
	static{
		storageMap = new HashMap<>();
		storageMap.put("stable", 50);
		storageMap.put("pen", 2000);
		storageMap.put("sty", 10);
	}

	public static void main(String[] args) {	
		Driver d = new Driver();
		List horseList = new ArrayList();
		
		while (true){
			try {
				Stable.addHorse(horseList, new Horse());
			} catch (SomeException e) {
				//System.out.println(e);
				System.out.println("That's it. No more Horse can be added!");
				break;
			}
			finally{
				System.out.println("I am from finally block");
			}
			
		}
		System.out.println("Number of horse added: " + horseList.size());
	}
}
