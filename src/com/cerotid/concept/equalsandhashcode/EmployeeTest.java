package com.cerotid.concept.equalsandhashcode;

import java.util.ArrayList;

public class EmployeeTest {
	private static ArrayList<Employee> empList = new ArrayList<>();
	
	public static void main(String[] args) {
		Employee emp1 = new Employee("David", 123);
		
		Employee emp2 = new Employee("David", 123);
		
		if(emp1.equals(emp2)) {
			System.out.println("We are equal");
		}
		else
			System.out.println("We are not equal!");
		
		
		addEmployees(emp1);
		addEmployees(emp2);
		
		System.out.println(empList.size());
		
	}

	private static void addEmployees(Employee employee) {
		if(! empList.contains(employee)) {
			empList.add(employee);
		}
	}
}
