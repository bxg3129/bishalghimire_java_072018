package com.cerotid.concept.equalsandhashcode;

public class ObjectComparison {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee e = new Employee("David", "Main st", 123456789, 123);

		Employee e2 = new Employee("David", "Main st", 123456789, 123);

		System.out.println(e);
		System.out.println(e2);

		System.out.println(e.hashCode());
		System.out.println(e2.hashCode());

		if (e.equals(e2)) {
			System.out.println("Two objects are equal");
		}
/*
		if (e.getAddress() != null) {
			if (e.getAddress().equals(e2.getAddress())) {
				System.out.println("I am here");
			}
		}*/
	}

}
