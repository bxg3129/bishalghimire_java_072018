package com.cerotid.concept.equalsandhashcode;

import java.util.HashSet;

public class HashCodeTest {
	public static void main(String[] args) {
		Employee employee1 = new Employee("David Smith", "121 Main St", 222111333, 101);
		Employee employee2 = new Employee("David Smith", "121 St", 222111333, 101);

		HashSet<Employee> employeeBucket = new HashSet<Employee>();
		employeeBucket.add(employee1);
		
		System.out.println(employeeBucket.contains(employee2));
		System.out.println(
				"employee.hashCode():  " + employee1.hashCode() + "  employee2.hashCode():" + employee2.hashCode());
	}
}
