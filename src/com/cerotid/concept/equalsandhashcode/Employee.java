package com.cerotid.concept.equalsandhashcode;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Employee implements Serializable {
	private String name;
	private String address;
	private int SSN;
	private int number;
	protected Date sDate;
	
	public Employee(){
		
	}
	

	public Employee(String name, int sSN) {
		super();
		this.name = name;
		SSN = sSN;
	}


	public Employee(String name, String address, int sSN, int number) {
		super();
		this.name = name;
		this.address = address;
		this.SSN = sSN;
		this.number = number;
	}


	public void studentInfo() {
		System.out.println("Student Name:  " + name + " " + address);
	}


	public String getName() {
		return name;
	}


	public String getAddress() {
		return address;
	}


	public int getSSN() {
		return SSN;
	}


	public int getNumber() {
		return number;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + SSN;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (SSN != other.SSN)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Employee [name=" + name + ", address=" + address + ", SSN=" + SSN + ", number=" + number + ", sDate="
				+ sDate + "]";
	}
	
	
}
