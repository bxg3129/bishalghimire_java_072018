package com.cerotid.client;

import java.util.Date;
import java.util.Scanner;

import com.cerotid.bank.controller.BankController;
import com.cerotid.bank.controller.BankingInterface;
import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;

public class BankUI {
	private BankingInterface bankingInterface;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BankUI bankUI = new BankUI();
		bankUI.setBankingInterface(new BankController());

		while (true) {
			displayMainMenu();

			bankUI.performActionBasedOnMenuChoice();
		}

	}

	private static void displayMainMenu() {
		System.out.println("Choose from the menu below:");
		System.out.println("1. Add Customer");
		System.out.println("2. Add Account");
		System.out.println("3. Send Money");
		System.out.println("4. Print Bank Status");
		System.out.println("5. Exit");
	}

	private void performActionBasedOnMenuChoice() {
		while (true) {
			try {
				switch (getScannerInstance().nextInt()) {
				case 1:
					addCustomer();
					break;
				case 2:
					addCustomerAccount();
					break;
				case 3:
					sendMoney();
					break;
				case 4:
					bankingInterface.printBankStatus();
					break;
				case 5:
					System.out.println("Thanks for visting out Bank!!");
					System.exit(0);
					break;
				default:
					System.out.println("Not in option!");
					break;
				}

				break; //breaks while loop when it execution point gets here
			} catch (Exception ex) {
				// Validation code
				System.out.println("Please provide valid integer input!");
			}
		}
	}

	private void sendMoney() {
		System.out.println("Money Sender Under construction!");
		
		Transaction transaction = createTransaction();
		
		sendMoney(transaction);
		
	}

	private void sendMoney(Transaction transaction) {
		// TODO Auto-generated method stub
		//TODO Money sender logic
		Customer customer = retrieveCustomerInformation();
		
		bankingInterface.sendMoney(customer, transaction);
	}

	private Transaction createTransaction() {
		// TODO Create transaction
		
		return null;
	}

	private void addCustomerAccount() {
		Customer customer = retrieveCustomerInformation();

		if(customer != null)
		{
			AccountType accntType = getCustomerChoiceAccountTypeToOpen();
	
			double openingBalance = getOpeningBalance();
	
			Account account = new Account(accntType, new Date(), openingBalance);
	
			customer.addAccount(account);
		}
		else
			System.out.println("Customer not available!");
	}

	private double getOpeningBalance() {
		System.out.println("Provide Opening Balance in Account: ");
		Scanner accountBalance = getScannerInstance();

		return accountBalance.nextDouble();
	}

	private void displayAccountTypeChoice() {
		System.out.println("Choose AccountType: [By default is Checkign account]: ");
		System.out.println("1. Checking 2. Saving 3. Business Checking");
	}

	private AccountType getCustomerChoiceAccountTypeToOpen() {
		displayAccountTypeChoice();

		AccountType accountType = null;

		Scanner accountTypeInput = getScannerInstance();
		switch (accountTypeInput.nextInt()) {
		case 1:
			accountType = AccountType.CHECKING;
			break;
		case 2:
			accountType = AccountType.SAVINGS;
			break;
		case 3:
			accountType = AccountType.BUSINESS_CHECKING;
			break;

		default:
			accountType = AccountType.CHECKING;
			break;
		}
		return accountType;
	}

	private Customer retrieveCustomerInformation() {
		Scanner accountInput = getScannerInstance();

		System.out.println("Enter Customer's SSN to retrieve His Account Information: ");
		String ssn = accountInput.next();

		return bankingInterface.getCustomerInfo(ssn);
	}

	private void addCustomer() {
		Scanner customerInput = getScannerInstance();

		System.out.println("Enter Customer First Name: ");
		String firstName = customerInput.next();
		System.out.println("Enter Customer Last Name: ");
		String lastName = customerInput.next();
		System.out.println("Enter Customer Social Security Number: ");
		String ssn = customerInput.next();
		System.out.println("Enter Customer Address:  ");
		String address = customerInput.next(); //TODO fix this defect #1

		Customer customer = new Customer(firstName, lastName, ssn, address);

		bankingInterface.addCustomer(customer);

		bankingInterface.printBankStatus();
	}

	private static Scanner getScannerInstance() {
		return new Scanner(System.in);
	}

	public BankingInterface getBankingInterface() {
		return bankingInterface;
	}

	public void setBankingInterface(BankingInterface bankingInterface) {
		this.bankingInterface = bankingInterface;
	}

}
